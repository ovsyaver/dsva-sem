# Semester project for DSVA ZS 23/24
# By Veronika Ovsyannikova

## CHAT
A distributed chat program using JSM and RabbitMQ for communication. The leader election is managed through the Bully algorithm.

## Cluster settings
This application uses RabbitMQ and expects two running brokers, one on hostname `dsvbox` and the second on hostname `dsvbox-1`.
Conf file for brokers is [rabbitmq.conf](src%2Fmain%2Fresources%2Frabbitmq.conf)(should be placed in `/etc/rabbitmq` folder on computer on which broker is running)
The user `vm` should be created in the broker cluster.

`sudo rabbitmqctl add_user vm vm
sudo rabbitmqctl set_user_tags vm administrator
sudo rabbitmqctl set_permissions -p / vm ".*" ".*" ".*"`

Command to set high availability of the cluster
`sudo rabbitmqctl set_policy ha-all ".*" '{"ha-mode":"all","ha-sync-mode":"automatic"}'`

## How to run
To run an instance, enter the following command in the terminal, where the first argument is the instance ID, and the second argument is
the instance name (used for private messaging).
The instance with the higher ID will become the leader.  
`mvn exec:java -Dexec.args="30 Stalker1299"`

To terminate an instance, enter the following command in the instance's terminal:  
`-die`

To send a message to the global chat room, enter the following command in the instance's terminal.  
`-m my message`

To send a private message, you need to know the instance name. Enter the instance name after -p.  
`-p instanceName -m my message`

To log out of the system (unsubscribe from all topics), enter the following command in the instance's terminal.  
`-logout`

To log in to the system (subscribe to all topics), enter the following command in the instance's terminal.  
`-login`

## Leader election
The Bully algorithm is used for leader election. When a new chat instance is created, or the leader instance dies thi election process starts.
Each instance acts as a producer and consumer of the `electionMessage` topic. During the election, instances send their IDs to this topic.
If an instance receives an ID higher than its own, it stops participating in the election and cannot become a leader.
If an instance does not receive a higher ID within 10 seconds of the election start, it declares itself the leader and sends a VICTORY message.

When an instance becomes the leader, it begins sending heartbeat messages every 5 seconds.
All instances are consumers of these messages.
If a chat instance detects no new heartbeat message from the leader within a 10-second period, it assumes the leader has failed and initiates a new election process.

## Messaging
Every message, whether to a global or private chat room, goes through the leader.
When a chat instance wants to send a message, it forwards it to the leader via the `leaderMessage` topic.

The leader then broadcasts the message to the global chat room through the `chatMessage` topic,
and all chat instances receive it as they are consumers of this topic.

Upon selecting a new leader and receiving the VICTORY message,
all chat instances send information about their identifiers (their private topics) to the leader,
so the leader knows where to send private messages.


## Logging
Logs are written in the console and saved into the file named `log` in source directory.
By Veronika Ovsyannikova
