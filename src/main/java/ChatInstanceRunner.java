import com.rabbitmq.client.Address;
import com.rabbitmq.jms.admin.RMQConnectionFactory;
import lombok.extern.slf4j.Slf4j;


import javax.jms.Connection;
import javax.jms.JMSException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

@Slf4j
public class ChatInstanceRunner {
    private static ChatInstance chatInstance;

    public static void main(String[] args) throws JMSException {
        RMQConnectionFactory connectionFactory = new RMQConnectionFactory();
        List<Address> addrArr = new ArrayList<>(Arrays.asList(
                new Address("dsvbox"),
                new Address("dsvbox-1")
        ));

        connectionFactory.setUsername("vm");
        connectionFactory.setPassword("vm");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");

        Connection connection = connectionFactory.createConnection(addrArr);


        if (args.length != 2) {
            log.error("Program should run with two arguments, first - instance id (int), second - instance name (string)");
            System.exit(1);
        }

        int instanceId = 0;
        try {
            instanceId = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            log.error("The first argument should be int");
            System.exit(1);
        }

        String instanceName = args[1];

        // Create and start a single chat instance
        chatInstance = new ChatInstance(connection, instanceId, instanceName);

        // Start a thread to listen for console input
        Thread consoleListenerThread = new Thread(ChatInstanceRunner::listenForShutdownCommand);
        consoleListenerThread.start();
    }

    private static void listenForShutdownCommand() {
        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                if (!scanner.hasNextLine()) {
                    continue; // Wait for next line of input
                }

                String input = scanner.nextLine();
                if ("-die".equals(input)) {
                    chatInstance.shutdown();
                    break;
                } else if (input.startsWith("-p")) {
                    Map<String, String> parsedArguments = parseArguments(input);
                    parsedArguments.forEach((key, value) -> {
                        if (value == null || value.isEmpty()) {
                            log.error("Error: Value for key '" + key + "' is " + (value == null ? "null" : "empty"));
                        }
                    });
                    chatInstance.sendPrivateMessage(parsedArguments.get("-p"), parsedArguments.get("-m"));
                } else if (input.startsWith("-m")) {
                    String message = parseArguments(input).get("-m");
                    if (message.isEmpty()) {
                        log.error("Message cannot be empty");
                    }
                    chatInstance.sendChatMessage(message);
                } else if ("-logout".equals(input)) {
                    chatInstance.logout();
                } else if ("-login".equals(input)) {
                    chatInstance.login();
                }
            }
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    private static Map<String, String> parseArguments(String input) {
        Map<String, String> args = new HashMap<>();
        String[] tokens = input.trim().split(" -");

        for (int i = 0; i < tokens.length; i++) {
            String token = tokens[i];
            if (!token.isEmpty()) {
                String[] pair = token.split(" ", 2);
                String key = i == 0 ? pair[0] : "-" + pair[0];
                String value = pair.length > 1 ? pair[1] : null;
                args.put(key, value);
            }
        }

        return args;
    }
}

