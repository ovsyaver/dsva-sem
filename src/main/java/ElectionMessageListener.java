import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.time.Instant;

@Slf4j
@RequiredArgsConstructor
public class ElectionMessageListener implements MessageListener {
    private final ChatInstance chatInstance;

    @Override
    public void onMessage(Message message) {
        String text;

        if (message instanceof TextMessage) {
            try {
                text = ((TextMessage) message).getText();
                if (text.startsWith("VICTORY")) {
                    log.info(text);

                    chatInstance.setupHeartbeatMonitoring();

                    chatInstance.sendInfoToTheLeader();
                    chatInstance.resetElectionState();

                } else {
                    chatInstance.stopHeartbeatMonitoring();
                    if (chatInstance.isLeader) {
                        chatInstance.unsetAsLeader();
                    }
                    int senderId = extractIdFromMessage(text);

                    if (senderId != chatInstance.getId() && !chatInstance.isHigherIdNodeResponded()) {
                        log.info("Received chat instance id " + senderId + " during election");
                    }

                    if (senderId > chatInstance.getId() && !chatInstance.isHigherIdNodeResponded()) {
                        log.info("Received higher id. This instance will not participate in election.");
                        chatInstance.setHigherIdNodeResponded(true);
                    }

                    else if (!chatInstance.isHigherIdNodeResponded()){
                        TextMessage response = chatInstance.getJmsSession().createTextMessage("ElectionMessage:" + chatInstance.getId());

                        if (!chatInstance.isDidSendId()) {
                            chatInstance.getTopicNameToProducerMap().get(ETopic.ELECTION).send(response);
                            chatInstance.setDidSendId(true);
                        }
                        chatInstance.waitForResponsesOrDeclareVictory();
                    }
                }

            } catch (JMSException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private Instant extractInstantFromMessage(String message) {
        String[] parts = message.split(":");
        return Instant.parse(parts[2]);
    }

    private int extractIdFromMessage(String message) {
        String[] parts = message.split(":");
        return Integer.parseInt(parts[1]);
    }
}
