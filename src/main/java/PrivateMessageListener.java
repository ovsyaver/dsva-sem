import lombok.extern.slf4j.Slf4j;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Slf4j
public class PrivateMessageListener implements MessageListener {
    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            String text;
            String[] parsedText;
            try {
                text = ((TextMessage) message).getText();
                parsedText = text.split(":");
            } catch (JMSException e) {
                throw new RuntimeException(e);
            }

            log.info("[Private chatroom with " + parsedText[0] + "] " + parsedText[0] + ":" + parsedText[1]);
        }
    }
}
