import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ETopic {
    BROADCAST_MESSAGE("chatMessage"),
    ELECTION("electionMessage"),
    HEARTBEAT("heartbeat"),
    LEADER("leaderMessage"),
    PRIVATE("");

    private final String topic;
}
