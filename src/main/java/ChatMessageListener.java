import lombok.RequiredArgsConstructor;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class ChatMessageListener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            String text;
            try {
                text = ((TextMessage) message).getText();
            } catch (JMSException e) {
                throw new RuntimeException(e);
            }

            System.out.println("[Global chatroom] " + text);
        }
    }

}

