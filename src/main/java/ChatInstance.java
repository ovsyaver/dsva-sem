import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
@Getter
@Setter
public class ChatInstance {
    private String instanceIdentifier;
    private Integer id;
    private String name;

    public boolean isLeader;

    private Connection jmsConnection;
    private Session jmsSession;

    private HashMap<ETopic, MessageProducer> topicNameToProducerMap = new HashMap<>();
    private HashMap<ETopic, MessageConsumer> topicNameToConsumerMap = new HashMap<>();

    private Destination chatMessageTopic;
    private Destination heartbeatTopic;
    private Destination electionMessageTopic;
    private Destination leaderMessageTopic;
    private Destination privateTopic;

    private Instant lastHeartbeatReceivedTime;
    private ScheduledExecutorService heartbeatMonitorExecutor;
    private ScheduledExecutorService heartbeatExecutor;
    private final long heartbeatInterval = 5;
    private final long electionTimeout = 1000; // 10 seconds timeout

    private boolean didSendId;
    private boolean higherIdNodeResponded;

    private Instant electionStartTime;
    private ScheduledExecutorService waitingForVictoryExecutor;

    private HashMap<String, String> nameToPrivateTopic = new HashMap<>();

    public ChatInstance(Connection connection, Integer id, String name) throws JMSException {
        instanceIdentifier = generateInstanceId();
        this.id = id;
        this.name = name;

        initializeJMS(connection);
        log.info("Instance of chat created with name " + name + " and id " + id + ", Instance identifier is " + instanceIdentifier);
        initiateLeaderElection();
    }

    // Initialize JMS Connection
    private void initializeJMS(Connection connection) {
        try {
            jmsConnection = connection;
            jmsSession = jmsConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create topics
            chatMessageTopic = jmsSession.createTopic(ETopic.BROADCAST_MESSAGE.getTopic());
            heartbeatTopic = jmsSession.createTopic(ETopic.HEARTBEAT.getTopic());
            leaderMessageTopic = jmsSession.createTopic(ETopic.LEADER.getTopic());
            electionMessageTopic = jmsSession.createTopic(ETopic.ELECTION.getTopic());
            privateTopic = jmsSession.createTopic(instanceIdentifier);

            createProducers();
            createConsumers();

            jmsConnection.start();
        } catch (JMSException e) {
            log.error(e.getMessage());
        }
    }

    public void setAsLeader() throws JMSException {
        this.isLeader = true;
        startHeartbeat();
    }

    public void unsetAsLeader() throws JMSException {
        this.isLeader = false;
        nameToPrivateTopic.clear();

        resetElectionState();
        stopHeartbeat();
    }

    public void sendChatMessage(String message) {
        String messageWithId = name + ": " + message;
        if (isLeader) {
            broadcastMessage(messageWithId);
        } else {
            // If not the leader, forward the message to the leader
            forwardMessageToLeader(messageWithId);
        }
    }

    public void broadcastMessage(String message) {
        try {
            TextMessage textMessage = jmsSession.createTextMessage(message);
            topicNameToProducerMap.get(ETopic.BROADCAST_MESSAGE).send(textMessage);
        } catch (JMSException e) {
            log.error(e.getMessage());
        }
    }

    public void forwardMessageToLeader(String message) {
        try {
            TextMessage textMessage = jmsSession.createTextMessage(message);
            topicNameToProducerMap.get(ETopic.LEADER).send(textMessage);
        } catch (JMSException e) {
            log.error(e.getMessage());
        }
    }

    void setupHeartbeatMonitoring() {
        heartbeatMonitorExecutor = Executors.newSingleThreadScheduledExecutor();
        heartbeatMonitorExecutor.scheduleAtFixedRate(this::checkHeartbeats, 5, 1, TimeUnit.SECONDS);
    }

    void stopHeartbeatMonitoring() {
        if (heartbeatMonitorExecutor != null) {
            heartbeatMonitorExecutor.shutdown();
        }
    }

    private void sendHeartbeat() {
        try {
            TextMessage heartbeatMessage = jmsSession.createTextMessage("Heartbeat from " + instanceIdentifier);
            topicNameToProducerMap.get(ETopic.HEARTBEAT).send(heartbeatMessage);
        } catch (JMSException e) {
            log.error(e.getMessage());
        }
    }

    public void startHeartbeat() throws JMSException {
        topicNameToProducerMap.put(ETopic.HEARTBEAT, jmsSession.createProducer(heartbeatTopic));

        heartbeatExecutor = Executors.newSingleThreadScheduledExecutor();
        heartbeatExecutor.scheduleAtFixedRate(this::sendHeartbeat, 0, heartbeatInterval, TimeUnit.SECONDS);
    }

    public void stopHeartbeat() throws JMSException {
        MessageProducer producer = topicNameToProducerMap.get(ETopic.HEARTBEAT);
        if (producer != null) {
            producer.close();
        }
        topicNameToProducerMap.remove(ETopic.HEARTBEAT);

        if (heartbeatExecutor != null && !heartbeatExecutor.isShutdown()) {
            heartbeatExecutor.shutdownNow();
        }
    }

    private void checkHeartbeats() {
        if (Duration.between(lastHeartbeatReceivedTime, Instant.now()).getSeconds() > 10) { // 10 seconds timeout for example
            try {
                log.info("Chat instance detected that leader died.");
                initiateLeaderElection();
            } catch (JMSException e) {
                log.error(e.getMessage());
            }
        }
    }

    public void updateLastHeartbeatReceived(Instant heartbeatTime) {
        lastHeartbeatReceivedTime = heartbeatTime;
    }

    private void initiateLeaderElection() throws JMSException {
        stopHeartbeatMonitoring();

        electionStartTime = Instant.now();
        log.info("This instance " + "with name " + name + " and id " + id + " started election at " + electionStartTime);
        TextMessage textMessage = jmsSession.createTextMessage("ElectionMessage:" + id + ":" + electionStartTime);
        topicNameToProducerMap.get(ETopic.ELECTION).send(textMessage);

        didSendId = true;
    }

    public void waitForResponsesOrDeclareVictory() {
        // Schedule a task to check after the timeout
        if (waitingForVictoryExecutor == null) {
            waitingForVictoryExecutor = Executors.newSingleThreadScheduledExecutor();
            waitingForVictoryExecutor.schedule(() -> {
                if (!higherIdNodeResponded) {
                    try {
                        declareVictory();
                    } catch (JMSException e) {
                        throw new RuntimeException(e);
                    }
                }
                waitingForVictoryExecutor.shutdown();
                waitingForVictoryExecutor = null;
            }, electionTimeout, TimeUnit.MILLISECONDS);
        }
    }

    private void declareVictory() throws JMSException {
        log.info("This chat instance is declared as Leader.");

        TextMessage message = jmsSession.createTextMessage("VICTORY: chat instance with name " + name + " and id " + id + " is declared as Leader");
        topicNameToProducerMap.get(ETopic.ELECTION).send(message);

        setAsLeader();
    }

    public void resetElectionState() {
        didSendId = false;
        higherIdNodeResponded = false;
    }

    public void login() {
        log.info("Chat instance with name " + name + " and id " + id + " is logged in.");

        resetElectionState();

        try {
            createProducers();
            createConsumers();

            initiateLeaderElection();
        } catch (JMSException e) {
            log.error(e.getMessage());
        }
    }

    public void logout() throws JMSException {
        log.info("Chat instance with name " + name + " and id " + id + " is logged out.");

        resetElectionState();

        stopHeartbeatMonitoring();
        nameToPrivateTopic.clear();

        try {
            closeAndClearProducers(topicNameToProducerMap);
            closeAndClearConsumers(topicNameToConsumerMap);
        } catch (JMSException e) {
            log.error(e.getMessage());
        }
    }

    private void closeAndClearProducers(HashMap<ETopic, MessageProducer> producers) throws JMSException {
        for (MessageProducer producer : producers.values()) {
            if (producer != null) {
                producer.close();
            }
        }
        producers.clear();
    }

    private void closeAndClearConsumers(HashMap<ETopic, MessageConsumer> consumers) throws JMSException {
        for (MessageConsumer consumer : consumers.values()) {
            if (consumer != null) {
                consumer.close();
            }
        }
        consumers.clear();
    }

    private void createConsumers() throws JMSException {
        MessageConsumer heartbeatConsumer = jmsSession.createConsumer(heartbeatTopic);
        MessageConsumer electionConsumer = jmsSession.createConsumer(electionMessageTopic);
        MessageConsumer broadcastConsumer = jmsSession.createConsumer(chatMessageTopic);
        MessageConsumer leaderConsumer = jmsSession.createConsumer(leaderMessageTopic);
        MessageConsumer privateConsumer = jmsSession.createConsumer(privateTopic);

        topicNameToConsumerMap.put(ETopic.HEARTBEAT, heartbeatConsumer);
        topicNameToConsumerMap.put(ETopic.ELECTION, electionConsumer);
        topicNameToConsumerMap.put(ETopic.BROADCAST_MESSAGE, broadcastConsumer);
        topicNameToConsumerMap.put(ETopic.LEADER, leaderConsumer);
        topicNameToConsumerMap.put(ETopic.PRIVATE, privateConsumer);

        // Set up message listeners
        heartbeatConsumer.setMessageListener(new HeartbeatMessageListener(this));
        electionConsumer.setMessageListener(new ElectionMessageListener(this));
        broadcastConsumer.setMessageListener(new ChatMessageListener());
        leaderConsumer.setMessageListener(new LeaderMessageListener(this));
        privateConsumer.setMessageListener(new PrivateMessageListener());
    }

    private void createProducers() throws JMSException {
        topicNameToProducerMap.put(ETopic.LEADER, jmsSession.createProducer(leaderMessageTopic));
        topicNameToProducerMap.put(ETopic.ELECTION, jmsSession.createProducer(electionMessageTopic));
        topicNameToProducerMap.put(ETopic.HEARTBEAT, jmsSession.createProducer(heartbeatTopic));
        topicNameToProducerMap.put(ETopic.BROADCAST_MESSAGE, jmsSession.createProducer(chatMessageTopic));
    }

    public void shutdown() throws JMSException {
        if (jmsConnection != null) {
            jmsConnection.close();
        }
        log.info("Chat Instance of chat created with name " + name + " and id " + id + "is shutting down..");
        System.exit(0);
    }

    private String generateInstanceId() {
        try {
            // Get local IP address
            InetAddress ip = InetAddress.getLocalHost();
            int port = 0;

            // Try to find an available port
            while (port == 0) {
                int potentialPort = new Random().nextInt(65535 - 49152) + 49152;
                if (isPortAvailable(potentialPort)) {
                    port = potentialPort;
                }
            }

            return ip.getHostAddress() + ":" + port;
        } catch (UnknownHostException e) {
            throw new RuntimeException("Error obtaining local IP address", e);
        }
    }

    private boolean isPortAvailable(int port) {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            serverSocket.setReuseAddress(true);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public void sendPrivateMessage(String receiver, String message) throws JMSException {
        String messageWithName = name + ": " + message;
        log.info("[Private chatroom with " + receiver + "] " + name + ": " + message);
        if (isLeader) {
            forwardPrivateMessage(receiver, messageWithName);
        } else {
            TextMessage textMessage = jmsSession.createTextMessage("PRIVATE:" + receiver + ":" + messageWithName);
            topicNameToProducerMap.get(ETopic.LEADER).send(textMessage);
        }
    }

    public void forwardPrivateMessage(String receiver, String message) throws JMSException {
        String privateTopicName = nameToPrivateTopic.get(receiver);

        if (privateTopicName == null) {
            log.error("Leader does not know instance with name " + receiver);
            return;
        }

        Destination privateTopic = jmsSession.createTopic(privateTopicName);
        MessageProducer producer = jmsSession.createProducer(privateTopic);

        TextMessage textMessage = jmsSession.createTextMessage(message);

        producer.send(textMessage);
        producer.close();
    }

    public void sendInfoToTheLeader() throws JMSException {
        log.info("Sending info about private topic to the leader");
        TextMessage message = jmsSession.createTextMessage("TOPIC:" + name + ":" + instanceIdentifier);
        topicNameToProducerMap.get(ETopic.LEADER).send(message);
    }

    public void addPrivateTopicToMap(String name, String privateTopic) {
        if (!nameToPrivateTopic.containsKey(name) || !Objects.equals(nameToPrivateTopic.get(name), privateTopic)) {
            log.info("Leader received info about private topic of " + name);
            nameToPrivateTopic.put(name, privateTopic);
        }

    }
}
