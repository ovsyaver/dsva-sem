import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Slf4j
@RequiredArgsConstructor
public class LeaderMessageListener implements MessageListener {
    private final ChatInstance chatInstance;

    @Override
    public void onMessage(Message message) {
        if (!chatInstance.isLeader) {
            return;
        }
        if (message instanceof TextMessage) {
            String text;
            String[] parsedText;
            try {
                text = ((TextMessage) message).getText();
                if (text.startsWith("PRIVATE")) {
                    parsedText = text.split(":");
                    chatInstance.forwardPrivateMessage(parsedText[1], parsedText[2] + ":" + parsedText[3]);
                }
                else if (text.startsWith("TOPIC")) {
                    parsedText = text.split(":");
                    chatInstance.addPrivateTopicToMap(parsedText[1], parsedText[2] + ":" + parsedText[3]);
                }
                else {
                    chatInstance.broadcastMessage(text);
                }
            } catch (JMSException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
