import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.time.Instant;

@Slf4j
@RequiredArgsConstructor
public class HeartbeatMessageListener implements MessageListener {
    private final ChatInstance chatInstance;

    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
//            String text;
//            try {
//                text = ((TextMessage) message).getText();
//
//            } catch (JMSException e) {
//                throw new RuntimeException(e);
//            }
            chatInstance.updateLastHeartbeatReceived(Instant.now());
        }
    }
}